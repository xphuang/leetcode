# -*- coding: utf-8 -*-
"""
Created on Sat Oct  6 09:05:34 2018

旋转图像
"""

class Solution:
    def rotate(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        n = len(matrix)
        for i in range(n // 2):
            for j in range(i, n - i - 1):
                left_upper, right_upper = matrix[i][j], matrix[j][n - i - 1]
                right_lower, left_lower = matrix[n - i - 1][n - j - 1], matrix[n - j - 1][i]
                matrix[i][j], matrix[j][n - i - 1], matrix[n - i - 1][n - j - 1], matrix[n - j - 1][i] = \
                left_lower, left_upper, right_upper, right_lower
        print(matrix)
        
