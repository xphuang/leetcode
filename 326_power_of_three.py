# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 09:53:18 2018

3的幂
"""

import math

class Solution:
    def isPowerOfThree(self, n):
        """
        :type n: int
        :rtype: bool
        """
        #注当n是3的幂，那么对3取log将会变成整数，细节是，如果用自然底数math.log不如math.log10精确
        return n > 0 and (math.log10(n) / math.log10(3)).is_integer()

