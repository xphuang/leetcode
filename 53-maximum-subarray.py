# -*- coding: utf-8 -*-
"""
Created on Sat Oct  6 19:18:48 2018

最大子序和
"""

import math

class Solution:
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        n, max_sum, cum_sum = len(nums), -math.inf, 0
        for i in range(n):
            if cum_sum + nums[i] > 0 and cum_sum > 0:
                cum_sum += nums[i]
            else:
                cum_sum = nums[i]
            max_sum = max(max_sum, cum_sum)
        return max_sum
    
    def maxSubArray2(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        n, max_sum, cum_sum = len(nums), -math.inf, 0
        for i in range(n):
            cum_sum += nums[i]
            if cum_sum > max_sum:
                max_sum = cum_sum
            if cum_sum < 0:
                cum_sum = 0
        return max_sum
