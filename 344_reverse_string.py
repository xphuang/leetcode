# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 21:36:58 2018

翻转字符串
"""

class Solution:
    def reverseString(self, s):
        """
        :type s: str
        :rtype: str
        """
        return s[::-1]

