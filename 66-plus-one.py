# -*- coding: utf-8 -*-
"""
Created on Sun Oct 14 09:20:21 2018

加一
"""

class Solution:
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        integer = int(''.join([str(_) for _ in digits]))
        integer += 1
        return [int(_) for _ in str(integer)]
