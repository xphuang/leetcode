# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 15:54:53 2018

有效的括号
"""

class Solution:
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        lookup = {')': '(', ']': '[', '}': '{'}
        parentheses_not_finished = []
        for i, c in enumerate(s):
            if c == "(" or c == "[" or c == "{":
                parentheses_not_finished.append(c)
            else:
                if len(parentheses_not_finished) == 0:
                    return False
                elif parentheses_not_finished.pop() != lookup[c]:
                    return False
        if len(parentheses_not_finished) > 0:
            return False
        return True
