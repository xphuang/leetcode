# -*- coding: utf-8 -*-
"""
Created on Sun Sep 30 09:12:37 2018

搜索旋转排序数组
"""

class Solution:
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        if not nums:
            return -1
        n = len(nums)
        if n == 1:
            if nums[0] == target:
                return 0
            else:
                return -1
        left, right = 0, n - 1
        while left < right:
            mid = int((left + right) / 2)
            print(nums[left], nums[mid], nums[right])
            if target == nums[left]:
                return left
            if target == nums[right]:
                return right
            if target == nums[mid]:
                return mid
            if nums[left] <= nums[right]:
                if target < nums[mid]:
                    right = mid - 1
                else:
                    left = mid + 1
            else:
                if nums[mid] <= nums[right]:
                    if target > nums[mid] and target < nums[right]:
                        left = mid + 1
#                    elif target < nums[mid]:
#                        right = mid - 1
                    else:
                        right = mid - 1
                else:
                    if target < nums[mid] and target > nums[left]:
                        right = mid - 1
#                    elif target > nums[mid]:
#                        left = mid + 1
                    else:
                        left = mid + 1
            
        return -1
            
        
        
