# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 21:49:41 2018

罗马数字转整数
"""

class Solution(object):
    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
#        num_dict = {1: "I", 5: "V", 10: "X", 50: "L", 100: "C", 500: "D", \
#                    1000: "M", 4: "IV", 9: 'IX', 40: 'XL', 90: "XC", \
#                    400: "CD", 900: "CM"}
#        s_dict = dict(zip([num_dict[_] for _ in num_dict], [_ for _ in num_dict]))
        s_dict = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000, 'IV': 4, 'IX': 9, 'XL': 40, 'XC': 90, 'CD': 400, 'CM': 900}
        if s in s_dict:
            return s_dict[s]
        num = 0
        n = len(s)
        i = 0
        while i < n:
            if i < n - 1 and s_dict[s[i]] < s_dict[s[i + 1]]:
                num += s_dict[s[i:i+2]]
                i += 2
            else:
                num += s_dict[s[i]]
                i += 1
        return num
