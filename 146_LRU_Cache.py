# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 18:19:26 2018

LRU缓存机制
"""

import collections

class LRUCache:

    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.capacity = capacity
        self.dict = collections.OrderedDict()
        

    def get(self, key):
        """
        :type key: int
        :rtype: int
        """
        if not key in self.dict:
            return -1
        else:
            val = self.dict[key]
            del self.dict[key]
            self.dict[key] = val
            return self.dict[key]
        
        

    def put(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: void
        """
        if key in self.dict:
            del self.dict[key]
        elif len(self.dict) == self.capacity:
            self.dict.popitem(last=False)
        self.dict[key] = value


# Your LRUCache object will be instantiated and called as such:
# obj = LRUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)
