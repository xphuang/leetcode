# -*- coding: utf-8 -*-
"""
Created on Sat Sep 29 20:14:10 2018

最长有效括号
"""

import collections

class Solution:
    def longestValidParentheses(self, s):
        """
        :type s: str
        :rtype: int
        """
        n = len(s)
        s_loc_align, left_parenthesis = [-1] * n, collections.deque()
        for i, c in enumerate(s):
            if c == "(":
                left_parenthesis.append(i)
            else:
                if len(left_parenthesis) == 0:
                    continue
                s_loc_align[left_parenthesis.pop()] = i
        max_len, i, continue_len = 0, 0, 0
        while i < n:
            if s_loc_align[i] == -1:
                i += 1
                continue_len = 0
                continue
            continue_len += s_loc_align[i] - i + 1
            max_len = max(max_len, continue_len)
            i = s_loc_align[i] + 1
        return max_len
    