# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 20:27:16 2018

矩阵置零
"""
from functools import reduce
class Solution:
    def setZeroes(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        nrow = len(matrix)
        ncol = len(matrix[0])
        first_row = reduce(lambda x, i: x or matrix[0][i] == 0, range(ncol), False)
        first_col = reduce(lambda x, j: x or matrix[j][0] == 0, range(nrow), False)
        
        for i in range(1, nrow):
            for j in range(1, ncol):
                if matrix[i][j] == 0:
                    matrix[i][0], matrix[0][j] = 0, 0
        
        for i in range(1, nrow):
            for j in range(1, ncol):
                if matrix[i][0] == 0 or matrix[0][j] == 0:
                    matrix[i][j] = 0
        
        if first_row:
            for i in range(ncol):
                matrix[0][i] = 0
        
        if first_col:
            for j in range(nrow):
                matrix[j][0] = 0
                    

