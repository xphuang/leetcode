# -*- coding: utf-8 -*-
"""
Created on Sun Sep 16 09:45:33 2018

最常见的单词
"""

import collections

class Solution:
    def mostCommonWord(self, paragraph, banned):
        """
        :type paragraph: str
        :type banned: List[str]
        :rtype: str
        """
        string = paragraph.lower()
        string_no_punctuation = [x for x in string if (x >= 'a' and x <= 'z') or x == " "]
        string2 = ''.join(string_no_punctuation)
        word_list = [x for x in string2.split() if x != ""]
        banned_set = set(banned)
        word_count = collections.Counter(word_list)
        word_count = collections.Counter([word.strip("!?',;.") for word in paragraph.lower().split()])
        word_sort = sorted(word_count.keys(), key = lambda x: word_count[x], reverse = True)
        for word in word_sort:
            if word in banned_set:
                continue
            return word
        
        
