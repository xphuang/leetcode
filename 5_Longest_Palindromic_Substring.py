# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 17:03:47 2018

最长回文子串
"""

class Solution:
    
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        n = len(s)
        max_string = ""
        for i in range(0, len(s)):
            flag = 0
            while i - flag >= 0 and i + flag < n:
                if s[i - flag] == s[i + flag]:
                    flag += 1
                else:
                    break
            cur_string = s[(i - flag + 1):(i + flag)]
            if len(cur_string) > len(max_string):
                max_string = cur_string
        for i in range(0, len(s) - 1):
            flag = 0
            while i - flag >= 0 and i + flag + 1 < n:
                if s[i - flag] == s[i + flag + 1]:
                    flag += 1
                else:
                    break
            cur_string = s[(i - flag + 1):(i + flag + 1)]
            if len(cur_string) > len(max_string):
                max_string = cur_string
        return max_string
            