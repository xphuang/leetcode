# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 18:24:21 2018

最小覆盖子串
"""

import collections

class Solution(object):
    def minWindow(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """
        n = len(s)
        m = len(t)
        l = m
        if n < m:
            return ""
        counter = collections.Counter(t)
        start = -1
        end = -1
        for i in range(n):
            if s[i] in counter:
                start = i
                end = i
                break
        if start == end == -1:
            return ""
        min_len = n + 1
        min_start = None
        min_end = None
        while True:
            while l > 0 and end < n:
                if s[end] in counter:
                    if counter[s[end]] > 0:
                        l -= 1
                    counter[s[end]] -= 1
                end += 1
            if l == 0 and min_len > end - start:
                min_len = end - start
                min_start = start
                min_end = end
            if start == n:
                break
            counter[s[start]] += 1
            if counter[s[start]] > 0:
                l += 1
            start += 1
            while start < n and not s[start] in counter:
                start += 1
            if start == n:
                break
        if min_start != None:
            return s[min_start:min_end]
        else:
            return ""
            
        
