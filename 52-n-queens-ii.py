# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 11:15:43 2018

N皇后 II
"""

class Solution:
    
    def queen(self, row, cur_arrange, n, result):
        if len(cur_arrange) == n:
            result.append(cur_arrange)
        else:
            for i in range(n):
                is_ok = True
                for arrange in cur_arrange:
                    if arrange[0] == row or arrange[1] == i or abs(arrange[0] - row) == abs(arrange[1] - i):
                        is_ok = False
                        break
                if is_ok:
                    self.queen(row + 1, cur_arrange + [(row, i)], n, result)
    
    def totalNQueens(self, n):
        """
        :type n: int
        :rtype: int
        """
        result = []
        self.queen(0, [], n, result)
        return len(result)
