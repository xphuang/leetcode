# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 11:11:49 2018

唯一摩尔斯密码词
"""

class Solution(object):
    def uniqueMorseRepresentations(self, words):
        """
        :type words: List[str]
        :rtype: int
        """
        morse_dict = dict(zip([chr(_) for _ in range(97, 123)], [".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."]))
        morse_set = set()
        for word in words:
            word_morse = ''.join([morse_dict[s] for s in word])
            morse_set.add(word_morse)
        return len(morse_set)
        
        