# -*- coding: utf-8 -*-
"""
Created on Sat Oct 13 22:17:23 2018

最小路径和
"""

class Solution:
    def minPathSum(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if not grid:
            return 0
        m, n = len(grid), len(grid[0])
        steps_min = [0] * n
        steps_min[0] = grid[0][0]
        for j in range(1, n):
            steps_min[j] = steps_min[j - 1] + grid[0][j]
        for i in range(1, m):
            steps_min[0] += grid[i][0]
            for j in range(1, n):
                steps_min[j] = min(steps_min[j - 1], steps_min[j]) + grid[i][j]
        return steps_min[-1]
        
