# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 17:00:18 2018

第二题：两数相加
"""

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        if l1 == None:
            return l2
        if l2 == None:
            return l1
        node1 = l1
        node2 = l2
        addnum = 0
        head = ListNode(0)
        l = head
        while True:
            if node1 != None and node2 != None:
                value = (node1.val + node2.val + addnum) % 10
                addnum = (node1.val + node2.val + addnum) // 10
                node1 = node1.next
                node2 = node2.next
            elif node1 == None and node2 != None:
                value = (node2.val + addnum) % 10
                addnum = (node2.val + addnum) // 10
                node2 = node2.next
            elif node1 != None and node2 == None:
                value = (node1.val + addnum) % 10
                addnum = (node1.val + addnum) // 10
                node1 = node1.next
            else:
                break
            l.next = ListNode(value)
            l = l.next
        if addnum != 0:
            l.next = ListNode(addnum)
        return head.next
            