# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 09:35:06 2018

字符串转整数 (atoi)
"""

class Solution(object):
    def myAtoi(self, str):
        """
        :type str: str
        :rtype: int
        """
        start = -1
        is_negative = False
        for i, c in enumerate(str):
            if c == " ":
                continue
            elif c != '-' and c != '+' and not (c >= '0' and c <= '9'):
                break
            elif c == "-" and i < len(str) - 1 and (str[i + 1] >= '0' and str[i + 1] <= '9'):
                is_negative = True
                start = i + 1
                break
            elif c == "+" and i < len(str) - 1 and (str[i + 1] >= '0' and str[i + 1] <= '9'):
                is_negative = False
                start = i + 1
                break
            elif c >= '0' and c <= '9':
                start = i
                break
            else:
                break
        if start == -1:
            return 0
        nums = []
        for i in range(start, len(str)):
            if str[i] <= '9' and str[i] >= '0':
                nums.append(str[i])
            else:
                break
        num = int(''.join(nums))
        if num > 0x7FFFFFFF and not is_negative:
            num = 0x7FFFFFFF
        if num > 0x7FFFFFFF and is_negative:
            num = 0x7FFFFFFF + 1
        if is_negative:
            num = -1 * num
        return num
        
