# -*- coding: utf-8 -*-
"""
Created on Wed Sep 26 16:37:11 2018

返回整数
"""


def return_integer(x):
    if x >= 0x7FFFFFFF:
        return 0x7FFFFFFF - 1
    elif x < -0x7FFFFFFF:
        return -0x7FFFFFFF
    else:
        return x

