# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 10:32:32 2018

N皇后
"""


class Solution:
    
    def queen(self, row, cur_arrange, n, result):
        if len(cur_arrange) == n:
            result.append(cur_arrange)
        else:
            for i in range(n):
                is_ok = True
                for arrange in cur_arrange:
                    if arrange[0] == row or arrange[1] == i or abs(arrange[0] - row) == abs(arrange[1] - i):
                        is_ok = False
                        break
                if is_ok:
                    self.queen(row + 1, cur_arrange + [(row, i)], n, result)
    
    def solveNQueens(self, n):
        """
        :type n: int
        :rtype: List[List[str]]
        """
        result, result_str = [], []
        self.queen(0, [], n, result)
        for i in range(len(result)):
            result_str_i = [['.'] * n for _ in range(n)]
            for j in range(n):
                result_str_i[result[i][j][0]][result[i][j][1]] = 'Q'
            result_str.append([''.join(result_str_i[_]) for _ in range(n)])
        return result_str
