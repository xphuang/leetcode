# -*- coding: utf-8 -*-
"""
Created on Sat Sep 22 17:37:28 2018

反转整数
"""

class Solution:
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        if x < 0:
            num = -int(str(-x)[::-1])
        else:
            num = int(str(x)[::-1])
        num = 0 if abs(num) > 0x7FFFFFFF else num
        return num

