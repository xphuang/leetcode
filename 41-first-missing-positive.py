# -*- coding: utf-8 -*-
"""
Created on Tue Oct  2 19:02:05 2018

缺失的第一个正数
"""

class Solution:
    
    def bucket_sort(self, nums):
        n = len(nums)
        for i in range(n):
            while nums[i] != i + 1:
                if nums[i] > n or nums[i] <= 0 or nums[i] == nums[nums[i] - 1]:
                    break
                temp = nums[i] 
                nums[i], nums[temp - 1] = nums[temp - 1], temp
    
    def firstMissingPositive(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        self.bucket_sort(nums)
        for i in range(len(nums)):
            if nums[i] != i + 1:
                return i + 1
        return len(nums) + 1
        