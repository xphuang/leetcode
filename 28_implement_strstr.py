# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 20:48:07 2018

实现strStr()
"""

class Solution:
    
    def strStr2(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        if needle == "":
            return 0
        for i in range(len(haystack) - len(needle) + 1):
            if haystack[i:i + len(needle)] == needle:
                return i
        return -1
        
    def getNext(self, pattern):
        n = len(pattern)
        nexts = [0] * n
        i, k, nexts[0] = 0, -1, -1
        while i < n - 1:
            if k == -1 or pattern[i] == pattern[k]:
                i, k = i + 1, k + 1
                if pattern[i] == pattern[k]:
                    nexts[i] = nexts[k]
                else:
                    nexts[i] = k
            else:
                k = nexts[k]
        return nexts
    
    def kmp(self, string, pattern):
        nexts = self.getNext(pattern)
        i, j = 0, 0
        while i < len(string) and j < len(pattern):
            if len(string) - i < len(pattern) - j:
                return -1
            if j == -1 or string[i] == pattern[j]:
                i, j = i + 1, j + 1
            else:
                j = nexts[j]
            if j == len(pattern):
                return i - len(pattern)
        return -1
    
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        if needle == "":
            return 0
        return self.kmp(haystack, needle)
        