# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 16:10:35 2018

括号的分数
"""

class Solution:
    def scoreOfParentheses(self, S):
        """
        :type S: str
        :rtype: int
        """
        score, depth = 0, 0
        for i in range(len(S)):
            if S[i] == "(":
                depth += 1
            else:
                depth -= 1
                if S[i - 1] == '(':
                    score += 2 ** depth
        return score
        