# -*- coding: utf-8 -*-
"""
Created on Tue Oct  2 22:05:12 2018

接雨水
"""
class Solution:
    def trap(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        n = len(height)
        left_increase_height, right_increase_height, initial_left, initial_right = [0], [n - 1], -1, -1
        for i in range(n):
            if height[i] >= initial_left:
                left_increase_height.append(i)
                initial_left = height[i]
        for i in range(n - 1, left_increase_height[-1] - 1, -1):
            if height[i] >= initial_right:
                right_increase_height.append(i)
                initial_right = height[i]
            
        sum_V = 0
        print(left_increase_height, right_increase_height)
        for i in range(1, len(left_increase_height)):
            for j in range(left_increase_height[i - 1] + 1, left_increase_height[i]):
                sum_V += (height[left_increase_height[i - 1]] - height[j])
        for i in range(1, len(right_increase_height)):
            for j in range(right_increase_height[i - 1] - 1, right_increase_height[i], -1):
                sum_V += (height[right_increase_height[i - 1]] - height[j])
        return sum_V
                
