# -*- coding: utf-8 -*-
"""
Created on Sat Oct  6 20:20:33 2018

螺旋矩阵
"""

class Solution:
    def spiralOrder(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        if not matrix:
            return []
        m, n, result = len(matrix), len(matrix[0]), []
        for i in range(min((m + 1) // 2, (n + 1) // 2)):
            if i == n - i - 1:
                for j in range(i, m - i):
                    result.append(matrix[j][n - i - 1])
                break
            elif i == m - i - 1:
                for j in range(i, n - i):
                    result.append(matrix[i][j])
                break
            else:
                for j in range(i, n - i - 1):
                    result.append(matrix[i][j])
                for j in range(i, m - i - 1):
                    result.append(matrix[j][n - i - 1])
                for j in range(n - i - 1, i, -1):
                    result.append(matrix[m - i - 1][j])
                for j in range(m - i - 1, i, -1):
                    result.append(matrix[j][i])
        return result
    
    def spiralOrder2(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        if not matrix:
            return []
        m, n, result = len(matrix), len(matrix[0]), []
        row, col, rank, flag, num = 0, -1, 0, 0, m * n
        while len(result) < num:
            if flag == 0:
                col += 1
                if col == n - rank - 1:
                    flag = 1
            elif flag == 1:
                row += 1
                if row == m - rank - 1:
                    flag = 2
            elif flag == 2:
                col -= 1
                if col == rank:
                    flag = 3
            else:
                row -= 1
                if row == rank + 1:
                    flag = 0
                    rank += 1
            result.append(matrix[row][col])
        return result
