# -*- coding: utf-8 -*-
"""
Created on Sun Oct 14 22:19:56 2018

爬楼梯
"""

class Solution:
    def climbStairs(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n == 1:
            return 1
        pre, pre_pre = 1, 1
        for i in range(1, n):
            temp = pre
            pre += pre_pre
            pre_pre = temp
        return pre
        
