# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 11:43:58 2018

合并区间
"""
#import math

# Definition for an interval.
class Interval:
    def __init__(self, s=0, e=0):
         self.start = s
         self.end = e

class Solution:
    def merge(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: List[Interval]
        """
        if not intervals:
            return []
        intervals.sort(key = lambda x: x.start)
        result = []
        result.append(intervals[0])
        for i in range(1, len(intervals)):
            interval = result.pop()
            start, end = interval.start, interval.end
            i_start, i_end = intervals[i].start, intervals[i].end
            if end < i_start:
                result.append(Interval(start, end))
                result.append(Interval(i_start, i_end))
            elif start < i_start and end > i_end:
                result.append(Interval(start, end))
            elif start < i_start and end <= i_end:
                result.append(Interval(start, i_end))
            elif start >= i_start and end > i_end:
                result.append(Interval(i_start, end))
            else:
                result.append(Interval(i_start, i_end))
        return result
                
                
