# -*- coding: utf-8 -*-
"""
Created on Thu Oct 11 15:15:32 2018

不同路径
"""
class Solution:
    
    def combinations_yanghui(self, n, large_integer):
        combinations_dict = dict()
        combinations_dict[(0, 0)] = 1
        for i in range(1, n + 1):
            combinations_dict[(i, 0)] = 1
            combinations_dict[(i, i)] = 1
            for j in range(1, i):
                combinations_dict[(i, j)] = (combinations_dict[(i - 1, j - 1)] + combinations_dict[(i - 1, j)]) % large_integer
        return combinations_dict
    
    def uniquePaths(self, m, n):
        """
        :type m: int
        :type n: int
        :rtype: int
        """
        if m + n <= 2:
            return 1
        combinations = [0] * (m + n - 1)
        for i in range(m + n - 1):
            combinations[0], combinations[i] = 1, 1
            for j in range(i - 1, 0, -1):
                combinations[j] += combinations[j - 1]
        return combinations[n - 1]


if __name__ == "__main__":
    sol = Solution()
    print(sol.uniquePaths(2, 6))
