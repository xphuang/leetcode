# -*- coding: utf-8 -*-
"""
Created on Sun Sep 30 13:34:02 2018

@author: XP
"""

class Solution:
    def searchRange(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        if not nums:
            return [-1, -1]
        n = len(nums)
        left, right, result = 0, n - 1, [-1, -1]
        while left < right:
            mid = (left + right) // 2
            if nums[mid] >= target:
                right = mid - 1
            else:
                left = mid + 1
        if right == -1:
            right = 0
        if nums[right] < target and right == n - 1:
            return [-1, -1]
        if nums[right] < target and nums[right + 1] == target:
            result[0] = right + 1
        if nums[right] == target:
            result[0] = right
        left, right = 0, n - 1
        while left < right:
            mid = (left + right) // 2
            if nums[mid] > target:
                right = mid - 1
            else:
                left = mid + 1
        if nums[left] == target:
            result[1] = left
        if nums[left] > target and left == 0:
            return [-1, -1]
        if nums[left] > target and nums[left - 1] == target:
            result[1] = left - 1
        return result
        
        