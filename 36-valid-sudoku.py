# -*- coding: utf-8 -*-
"""
Created on Sun Sep 30 16:33:38 2018

有效的数独
"""

class Solution:
    def isValidSudoku(self, board):
        """
        :type board: List[List[str]]
        :rtype: bool
        """
        for i in range(9):
            num_list = [_ for _ in board[i] if _ != '.']
            num_set = set(num_list)
            if len(num_set) != len(num_list):
                return False
        for i in range(9):
            num_list = [board[j][i] for j in range(9) if board[j][i] != '.']
            num_set = set(num_list)
            if len(num_set) != len(num_list):
                return False
        for i in range(3):
            for j in range(3):
                num_list = [board[k][l] for k in range(3 * i, 3 * i + 3) for \
                            l in range(3 * j, 3 * j + 3) if board[k][l] != '.']
                num_set = set(num_list)
                if len(num_set) != len(num_list):
                    return False
        return True
