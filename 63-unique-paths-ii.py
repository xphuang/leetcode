# -*- coding: utf-8 -*-
"""
Created on Sat Oct 13 21:41:49 2018

不同路径 II
"""

class Solution:
    def uniquePathsWithObstacles(self, obstacleGrid):
        """
        :type obstacleGrid: List[List[int]]
        :rtype: int
        """
        if not obstacleGrid:
            return 1
        m, n = len(obstacleGrid), len(obstacleGrid[0])
        steps = [0] * n
        for j in range(n):
            if obstacleGrid[0][j] == 1:
                break
            steps[j] = 1
        for i in range(1, m):
            if obstacleGrid[i][0] == 1:
                steps[0] = 0
            for j in range(1, n):
                if obstacleGrid[i][j] == 1:
                    steps[j] = 0
                else:
                    steps[j] += steps[j - 1]
        return steps[-1]
