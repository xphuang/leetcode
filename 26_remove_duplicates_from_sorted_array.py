# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 16:39:37 2018

删除排序数组中的重复项
"""

class Solution:
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if not nums:
            return 0
        diff = 1
        for i in range(1, len(nums)):
            if nums[i] == nums[i - 1]:
                continue
            nums[diff] = nums[i]
            diff += 1
        return diff
        
