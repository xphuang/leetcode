# -*- coding: utf-8 -*-
"""
Created on Sat Sep 29 12:31:56 2018

下一个排列
"""

class Solution:
    def nextPermutation(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        flag, n = -1, len(nums)
        for i in range(n - 1):
            if nums[i] < nums[i + 1]:
                flag = i
        if flag == -1:
#            for i in range(int(n / 2)):
#                nums[i], nums[n - i - 1] = nums[n - i - 1], nums[i]
            nums.reverse()
        else:
            min_more_index, min_more = flag + 1, nums[flag + 1]
            for i in range(flag + 2, n):
                if nums[i] > nums[flag] and nums[i] <= min_more:
                    min_more_index, min_more = i, nums[i]
                else:
                    break
            nums[flag], nums[min_more_index] = nums[min_more_index], nums[flag]
#            for i in range(flag + 1, int((n - 1 - flag) / 2 + flag + 1)):
#                nums[i], nums[n - i + flag] = nums[n - i + flag], nums[i]
            a = nums[flag + 1:]
            nums[flag + 1:] = a[::-1]
        print(nums)

