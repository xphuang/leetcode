# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 21:48:45 2018

反转字符2
"""
class Solution:
    def reverseStr(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: str
        """
        n = len(s)
        a = n // (2 * k)
        b = n % (2 * k)
        c = b // k
        if c > 0:
            string_list = [s[((2 * x - 2) * k) : ((2 * x - 1) * k)][::-1] + \
                   s[((2 * x - 1) * k) : ((2 * x) * k)] for x in range(1, a + 1)] + \
                   [s[(2 * k * a ) : (2 * k * a + k)][::-1]] + [s[(2 * k * a + k): n]]
        else:
            string_list = [s[((2 * x - 2) * k) : ((2 * x - 1) * k)][::-1] + \
                   s[((2 * x - 1) * k) : ((2 * x) * k)] for x in range(1, a + 1)] + \
                   [s[(2 * k * a): n][::-1]]
        return ''.join(string_list)
