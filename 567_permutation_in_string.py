# -*- coding: utf-8 -*-
"""
Created on Sun Sep 16 11:21:19 2018

字符串的排列
"""

import collections

class Solution:
    def checkInclusion(self, s1, s2):
        """
        :type s1: str
        :type s2: str
        :rtype: bool
        """
        counter = collections.Counter(s1)
        l = len(s1)
        for i in range(len(s2)):
            if counter[s2[i]] > 0:
                l -= 1
            counter[s2[i]] -= 1
            if l == 0:
                return True
            start = i - len(s1) + 1
            if start >= 0:
                counter[s2[start]] += 1
                if counter[s2[start]] > 0:
                    l += 1
        return False

