# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 22:06:06 2018

最长公共前缀
"""

class Solution:
    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        """
#        strs.sort()
        if len(strs) == 0:
            return ''
        common_len = 1
        while True:
            if common_len > len(strs[0]):
                common_len -= 1
                break
            prefix = strs[0][:common_len]
            flag = 0
            for i in range(1, len(strs)):
                if common_len > len(strs[i]):
                    common_len -= 1
                    flag = 1
                    break
                elif prefix == strs[i][:common_len]:
                    continue
                else:
                    common_len -= 1
                    flag = 1
                    break
            if flag == 0:
                common_len += 1
            else:
                break
        if common_len == 0:
            return ""
        return strs[0][:common_len]
    
    def longestCommonPrefix2(self, strs):
        if not strs:
            return ""
        for i in range(len(strs[0])):
            for string in strs[1:]:
                if i >= len(string) or string[i] != strs[0][i]:
                    return strs[0][:i]
        return strs[0]
        
