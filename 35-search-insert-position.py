# -*- coding: utf-8 -*-
"""
Created on Sun Sep 30 16:06:27 2018

@author: XP
"""

class Solution:
    def searchInsert(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        n = len(nums)
        left, right = 0, n - 1
        while left <= right:
            mid = (left + right) // 2
            if nums[mid] == target:
                return mid
            elif nums[mid] < target:
                left = mid + 1
            else:
                right = mid - 1
        print(left, mid, right)
        if nums[mid] < target:
            return mid + 1
        return mid
        