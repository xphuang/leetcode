# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 08:42:47 2018

字符串相乘
"""

import collections

class Solution:
    def multiply(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        if (len(num1) == 1 and int(num1[0]) == 0) or (len(num2) == 1 and int(num2[0]) == 0):
            return '0'
        num_list1, num_list2, m, n = [int(_) for _ in num1], [int(_) for _ in num2], len(num1), len(num2)
        sum_all, cur_sum = collections.deque(), collections.deque()
        for j in range(n):
            num, carry = num_list2[n - 1 - j], 0
            for i in range(m):
                cur_num = carry + num * num_list1[m - i - 1]
                cur_sum.appendleft(cur_num % 10)
                carry = cur_num // 10
            if carry > 0:
                cur_sum.appendleft(carry)
            k, carry = 0, 0
            while len(cur_sum) > 0 or k < len(sum_all):
                new_num, existing_num = 0, 0
                if k >= j and len(cur_sum) > 0:
                    new_num = cur_sum.pop()
                if k < len(sum_all):
                    existing_num = sum_all[len(sum_all) - 1 - k]
                cur_num = new_num + existing_num + carry
                if k < len(sum_all):
                    sum_all[len(sum_all) - 1 - k] = cur_num % 10
                else:
                    sum_all.appendleft(cur_num % 10)
                carry = cur_num // 10
                k += 1
            if carry > 0:
                sum_all.appendleft(carry)
        return ''.join([str(_) for _ in sum_all])
    
    def multiply2(self, num1, num2):
        num1, num2 = num1[::-1], num2[::-1]
        res = [0] * (len(num1) + len(num2))
        for i in range(len(num1)):
            for j in range(len(num2)):
                num = int(num1[i]) * int(num2[j])
                res[i + j] += num
                res[i + j + 1] += res[i + j] // 10
                res[i + j] = res[i + j] % 10
        i = len(res) - 1
        while i > 0 and res[i] == 0:
            i -= 1
        return ''.join([str(_) for _ in res[i::-1]])
