# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 11:01:50 2018

无重复字符的最长子串
"""

class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        start = -1
        string_loc = dict()
        max_len = 0
        for i, c in enumerate(s):
            if c in string_loc and string_loc[c] > start:
                start = string_loc[c]
                string_loc[c] = i
            else:
                string_loc[c] = i
                max_len = max(max_len, i - start)
        return max_len
    