# -*- coding: utf-8 -*-
"""
Created on Wed Sep 26 16:19:18 2018

两数相除
"""

class Solution:
    
    def return_integer(self, x):
        if x >= 0x7FFFFFFF:
            return 0x7FFFFFFF
        elif x < -0x7FFFFFFF - 1:
            return -0x7FFFFFFF - 1
        else:
            return x
    
    def divide(self, dividend, divisor):
        """
        :type dividend: int
        :type divisor: int
        :rtype: int
        """
        if dividend == 0:
            return 0
        is_negative = (dividend > 0 and divisor < 0) or (dividend < 0 and divisor > 0)
        dividend, divisor = abs(dividend), abs(divisor)
        quotient, divisor_origin, flag = 0, divisor, 1
        while dividend >= divisor_origin:
            divisor, flag = divisor_origin, 1
            while dividend >= divisor:
                dividend -= divisor
                quotient += flag
                divisor += divisor
                flag += flag
        if is_negative:
            return self.return_integer(-quotient)
        return self.return_integer(quotient)
        
        

