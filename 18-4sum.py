# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 15:04:52 2018

四数之和
"""

class Solution:
    
    def fourSum_visit(self, nums, cur_tuple, cur_loc, cursum, tuple_list, target):
        if cursum == target and len(cur_tuple) == 4:
            tuple_list.append(cur_tuple)
        elif len(cur_tuple) < 4:
            for i in range(cur_loc + 1, len(nums)):
                if (i > cur_loc + 1 and nums[i] == nums[i - 1]):
                    continue
                if cursum + nums[i] > target and nums[i] > 0:
                    break
                self.fourSum_visit(nums, cur_tuple + [nums[i]], i, cursum + nums[i], tuple_list, target)
        
    
    def fourSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        nums.sort()
        tuple_list = []
        for i in range(len(nums)):
            if (i > 0 and nums[i] == nums[i - 1]):
                continue
            if nums[i] > target and nums[i] > 0:
                break
            self.fourSum_visit(nums, [nums[i]], i, nums[i], tuple_list, target)
        return tuple_list
    
    def threeSum2(self, nums, start, target):
        nums.sort()
        tuple_list = []
        i = start
        while i < len(nums) - 2:
            if i == start or nums[i] != nums[i - 1]:
                j, k = i + 1, len(nums) - 1
                while j < k:
                    if nums[i] + nums[j] + nums[k] < target:
                        j += 1
                    elif nums[i] + nums[j] + nums[k] > target:
                        k -= 1
                    else:
                        tuple_list.append([nums[i], nums[j], nums[k]])
                        j, k = j + 1, k - 1
                        while j < k and nums[j] == nums[j - 1]:
                            j += 1
                        while j < k and nums[k] == nums[k + 1]:
                            k -= 1
            i += 1
        return tuple_list
    
    def fourSum2(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        if len(nums) < 4:
            return []
        nums.sort()
        tuple_list = []
        for i in range(len(nums) - 3):
            if i > 0 and nums[i] == nums[i - 1]:
                continue
            for three_comb in self.threeSum2(nums, i + 1, target - nums[i]):
                tuple_list.append([nums[i]] + three_comb)
        return tuple_list

