# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 21:12:29 2018

整数转罗马数字
"""

class Solution(object):
    def intToRoman(self, num):
        """
        :type num: int
        :rtype: str
        """
        num_dict = {1: "I", 5: "V", 10: "X", 50: "L", 100: "C", 500: "D", \
                    1000: "M", 4: "IV", 9: 'IX', 40: 'XL', 90: "XC", \
                    400: "CD", 900: "CM"}
        if num in num_dict:
            return num_dict[num]
        divisors = list(num_dict.keys())
        divisors.sort(reverse = True)
        num_list = []
        for i in range(len(divisors)):
            if num in num_dict:
                num_list.append(num_dict[num])
                break
            quotient = num // divisors[i]
            remainder = num % divisors[i]
            cur = quotient * divisors[i]
            if cur in num_dict:
                num_list.append(num_dict[cur])
            else:
                num_list.append(quotient * num_dict[divisors[i]])
            if remainder == 0:
                break
            num = remainder
        return ''.join(num_list)
