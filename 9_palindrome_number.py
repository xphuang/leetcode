# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 10:06:52 2018

回文数
"""

class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        xstr = str(x)
        return xstr == xstr[::-1]

