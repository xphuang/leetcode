# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 14:29:04 2018

电话号码的字母组合
"""

class Solution:
    
    def digits_visit(self, digits, digit_dict, loc, current_comb, letter_comb):
        if loc == len(digits) - 1:
            for c in digit_dict[digits[loc]]:
                letter_comb.append(current_comb + c)
        else:
            for c in digit_dict[digits[loc]]:
                self.digits_visit(digits, digit_dict, loc + 1, current_comb + c, letter_comb)
    
    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        if digits == "":
            return []
        digit_dict = {'2': 'abc', '3': 'def', '4':'ghi', '5': 'jkl', '6': 'mno', \
                      '7': 'pqrs', '8': 'tuv', '9': 'wxyz'}
        letter_comb = []
        self.digits_visit(digits, digit_dict, 0, '', letter_comb)
        return letter_comb
        
        