# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 16:46:02 2018

最后一个单词的长度
"""

class Solution:
    def lengthOfLastWord(self, s):
        """
        :type s: str
        :rtype: int
        """
        s_strip = s.strip()
        last_len, n = 0, len(s_strip)
        for i in range(n):
            if s_strip[n - 1 - i] == ' ':
                break
            last_len += 1
        return last_len
        
        