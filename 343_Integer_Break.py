# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 15:46:01 2018

整数拆分
"""

class Solution:
    def integerBreak(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n == 0:
            return 0
        if n == 1:
            return 0
        A = [0] * n
        A[0] = 0
        A[1] = 1
        for i in range(2, n):
            A[i] = max([max(j * A[i - j], j * (i + 1 - j)) for j in range(1, i)])
        return A[-1]

