# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 16:06:54 2018

括号生成
"""

class Solution:
    
    def parenthesis_visit(self, m1, m2, cur_parenth, parenthesis_list):
        if m1 == 0 and m2 == 1:
            parenthesis_list.append(cur_parenth + ')')
        elif m1 < m2 and m1 > 0:
            self.parenthesis_visit(m1 - 1, m2, cur_parenth + '(', parenthesis_list)
            self.parenthesis_visit(m1, m2 - 1, cur_parenth + ')', parenthesis_list)
        elif m1 == m2:
            self.parenthesis_visit(m1 - 1, m2, cur_parenth + '(', parenthesis_list)
        elif m1 == 0 and m2 > 1:
            self.parenthesis_visit(m1, m2 - 1, cur_parenth + ')', parenthesis_list)
    
    def generateParenthesis(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        if n == 0:
            return []
        parenthesis_list = []
        self.parenthesis_visit(n, n, '', parenthesis_list)
        return parenthesis_list
        