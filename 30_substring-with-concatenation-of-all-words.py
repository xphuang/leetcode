# -*- coding: utf-8 -*-
"""
Created on Wed Sep 26 17:08:58 2018

与所有单词相关联的字串
"""

import collections

class Solution:
    def findSubstring(self, s, words):
        """
        :type s: str
        :type words: List[str]
        :rtype: List[int]
        """
        if not words:
            return []
        result, n, word_len, words_num = [], len(s), len(words[0]), len(words)
        word_count_origin = collections.Counter(words)
        if words_num * word_len > n:
            return []
        for i in range(n - words_num * word_len + 1):
            j, l, word_count_now = i, words_num, collections.Counter(dict())
            while j < i + words_num * word_len:
                word = s[j:j + word_len]
                if word_count_origin[word] > word_count_now[word]:
                    l -= 1
                    word_count_now[word] += 1
                else:
                    break
                j += word_len
            if l == 0:
                result.append(i)
#            for word in word_count_origin:
#                word_count_now[word] = word_count_origin[word]
        return result
        

