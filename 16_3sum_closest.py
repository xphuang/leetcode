# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 11:59:46 2018

最接近的三数之和
"""

class Solution:
    def threeSumClosest(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        if len(nums) < 3:
            return []
        nums.sort()
        i, min_diff, min_list = 0, abs(sum(nums[:3]) - target), [nums[0], nums[1], nums[2]]
        while i < len(nums) - 2:
            if i == 0 or nums[i] != nums[i - 1]:
                j, k = i + 1, len(nums) - 1
                while j < k:
                    sums = nums[i] + nums[j] + nums[k]
                    if abs(sums - target) < min_diff:
                        min_list[0], min_list[1], min_list[2] = nums[i], nums[j], nums[k]
                        min_diff = abs(sums - target)
                    if sums < target:
                        j += 1
                    elif sums > target:
                        k -= 1
                    else:
                        min_list[0], min_list[1], min_list[2] = nums[i], nums[j], nums[k]
                        return target
#                        j, k = j + 1, k - 1
#                        while j < k and nums[j] == nums[j - 1]:
#                            j += 1
#                        while j < k and nums[k] == nums[k + 1]:
#                            k -= 1
            i += 1
        return sum(min_list)
        
        