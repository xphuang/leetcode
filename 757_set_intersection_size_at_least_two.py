# -*- coding: utf-8 -*-
"""
Created on Mon Sep 17 10:13:24 2018

设置交集大小至少为2
"""

class Solution:
    def intersectionSizeTwo(self, intervals):
        """
        :type intervals: List[List[int]]
        :rtype: int
        """
        sorted_intervals = sorted(intervals, key = lambda x: (x[0], -x[-1]))
        cnts = [2] * len(sorted_intervals)
        result = 0
        while sorted_intervals:
            (start, _), cnt = sorted_intervals.pop(), cnts.pop()
            for s in range(start, start + cnt):
                for i in range(len(sorted_intervals)):
                    if cnts[i] and s <= sorted_intervals[i][1]:
                        cnts[i] -= 1
            result += cnt
        return result
        
