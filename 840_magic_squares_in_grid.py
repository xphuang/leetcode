# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 10:14:04 2018

矩阵中的幻方
"""

class Solution:
    
    def is_sub_magic(self, i, j, grid):
        if set([grid[x][y] for x in range(i - 1, i + 2) for y in range(j - 1, j + 2)]) != set(range(1, 10)):
            return False
        flag = \
        grid[i - 1][j - 1] + grid[i - 1][j] + grid[i - 1][j + 1] \
        == grid[i][j - 1] + grid[i][j] + grid[i][j + 1] \
        == grid[i + 1][j - 1] + grid[i + 1][j] + grid[i + 1][j + 1] \
        == grid[i - 1][j - 1] + grid[i][j - 1] + grid[i + 1][j - 1] \
        == grid[i - 1][j] + grid[i][j] + grid[i + 1][j] \
        == grid[i - 1][j + 1] + grid[i][j + 1] + grid[i + 1][j + 1] \
        == grid[i - 1][j - 1] + grid[i][j] + grid[i + 1][j + 1] \
        == grid[i - 1][j + 1] + grid[i][j] + grid[i + 1][j - 1]
        return flag and grid[i][j] == 5
    
    def numMagicSquaresInside(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        if len(grid) < 3:
            return 0
        N = len(grid)
        result = 0
        for i in range(1, N - 1):
            for j in range(1, N - 1):
                result += self.is_sub_magic(i, j, grid)
        return result
    