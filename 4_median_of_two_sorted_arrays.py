# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 12:28:35 2018

两个排序数组的中位数
"""

import bisect

class Solution(object):
    
    def is_successful_right(self, mid_more, nums1, nums2, mid1, mid2):
        mid1_more = len(nums2) - mid2 + len(nums1) - mid1 - 1
        if mid1_more == mid_more:
            return 0
        elif mid1_more < mid_more:
            return -1
        else:
            return 1
        
    
    def is_successful_left(self, mid_less, mid_more, nums1, nums2, mid1, mid2):
        mid1_less = mid1 + mid2
        if mid1_less == mid_less:
            return 0
        elif mid1_less < mid_less:
            return -1
        else:
            return 1
    
    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """
        left1, right1, left2, right2 = 0, len(nums1) - 1, 0, len(nums2) - 1
        if len(nums1) + len(nums2) % 2 == 0:
            medianloc1 = (len(nums1) + len(nums2)) // 2
            medianloc2 = medianloc1 - 1
            mid_less = medianloc2
            mid_more = mid_less
        else:
            medianloc1 = (len(nums1) + len(nums2)) // 2
            medianloc2 = medianloc1
            mid_less = medianloc1 - 1
            mid_more = mid_less
            
        #寻找左边第一个
        while True:
            mid1 = int((left1 + right1) / 2)
            #从nums2里面查找mid1的位置
            mid2 = bisect.bisect_right(nums2, nums1[mid1], left1, right1)
            
        
        
        
        
        