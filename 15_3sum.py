# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 22:31:37 2018

@author: XP
"""

class Solution:
    
    def threeSum_visit(self, nums, cur_tuple, cur_loc, cursum, tuple_list):
        if cursum == 0 and len(cur_tuple) == 3:
            tuple_list.append(cur_tuple)
        elif len(cur_tuple) < 3:
            for i in range(cur_loc + 1, len(nums)):
                if (i > cur_loc + 1 and nums[i] == nums[i - 1]):
                    continue
                if cursum + nums[i] > 0:
                    break
                self.threeSum_visit(nums, cur_tuple + [nums[i]], i, cursum + nums[i], tuple_list)
        
    
    def threeSum2(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        nums.sort()
        tuple_list = []
        for i in range(len(nums)):
            if (i > 0 and nums[i] == nums[i - 1]):
                continue
            if nums[i] > 0:
                break
            self.threeSum_visit(nums, [nums[i]], i, nums[i], tuple_list)
        return tuple_list
    
    def threeSum(self, nums):
        nums.sort()
        tuple_list = []
        i = 0
        while i < len(nums) - 2:
            if nums[i] > 0:
                break
            if i == 0 or nums[i] != nums[i - 1]:
                j, k = i + 1, len(nums) - 1
                while j < k:
                    if nums[i] + nums[j] + nums[k] < 0:
                        j += 1
                    elif nums[i] + nums[j] + nums[k] > 0:
                        k -= 1
                    else:
                        tuple_list.append([nums[i], nums[j], nums[k]])
                        j, k = j + 1, k - 1
                        while j < k and nums[j] == nums[j - 1]:
                            j += 1
                        while j < k and nums[k] == nums[k + 1]:
                            k -= 1
            i += 1
        return tuple_list
    