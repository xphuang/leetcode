# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 10:49:48 2018

两数之和
"""

class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        nums_loc = dict()
        for i, num in enumerate(nums):
            num_diff = target - num
            if num_diff in nums_loc:
                return [nums_loc[num_diff], i]
            nums_loc[num] = i
        return []
