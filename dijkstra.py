# -*- coding: utf-8 -*-
"""
Created on Wed Sep 26 09:35:43 2018

最短路径dijkstra算法
"""

import math

def dijkstra(adj, adjweight, start):
    n = len(adj)
    distances = [math.inf for _ in range(n)]
    previous = [None for _ in range(n)]
    current_node = start
    distances[current_node] = 0
    temp_shortest_dis = [math.inf for _ in range(n)]
    temp_shortest_dis[current_node] = 0
    shortest_nodes = set()
    shortest_nodes.add(current_node)
    while len(shortest_nodes) < n:
        for i in range(len(adj[current_node])):
            adjnode = adj[current_node][i]
            if adjnode in shortest_nodes:
                continue
            adjnode_weight = adjweight[current_node][i]
            if temp_shortest_dis[adjnode] > distances[current_node] + adjnode_weight:
                temp_shortest_dis[adjnode] = distances[current_node] + adjnode_weight
                previous[adjnode] = current_node
        min_shortest = math.inf
        min_node = None
        for i in range(n):
            if i in shortest_nodes:
                continue
            if min_shortest > temp_shortest_dis[i]:
                min_shortest = temp_shortest_dis[i]
                min_node = i
        if min_node == None:
            break
        else:
            distances[min_node] = temp_shortest_dis[min_node]
            shortest_nodes.add(min_node)
            current_node = min_node
    return (distances, previous)
