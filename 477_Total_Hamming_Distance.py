# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 20:15:17 2018

汉明距离总和
"""

class Solution:
    def totalHammingDistance(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        bin_nums = []
        n = len(nums)
        max_len = 0
        for i in range(n):
            bnum = bin(nums[i])[2:]
            max_len = max(max_len, len(bnum))
            bin_nums.append(bnum)
        for i in range(n):
            bin_nums[i] = '0' * (max_len - len(bin_nums[i])) + bin_nums[i]
        hamming_zero_nums = [sum([bin_nums[y][x] == "0" for y in range(n)]) for x in range(max_len)]
        return sum([(n - hamming_zero_nums[j]) * hamming_zero_nums[j] for j in range(max_len)])
