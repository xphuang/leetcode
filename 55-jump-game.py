# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 11:34:38 2018

跳跃游戏
"""
class Solution:
    def canJump(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        if not nums:
            return True
        n = len(nums)
        min_step, max_step, flag = 1, min(nums[0], n - 1), 1
        while True:
            new_max = -1
            for i in range(min_step, max_step + 1):
                new_max = max(new_max, i + nums[i])
            flag += 1
            min_step = max_step + 1
            if min_step >= n:
                return True
            if new_max <= max_step:
                break
            max_step = new_max
            if max_step >= n:
                return True
        if max_step >= n:
            return True
        return False
