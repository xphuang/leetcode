# -*- coding: utf-8 -*-
"""
Created on Fri Oct  5 20:36:39 2018

跳跃游戏 II
"""

import math
import collections

class Solution:
    def jump(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        n, distances = len(nums), dict()
        for i in range(n):
            distances[(i, i)] = 0
            for j in range(nums[i]):
                if i + j + 1 > n - 1:
                    break
                distances[(i, i + j + 1)] = 1
        for delta in range(1, n):
            for i in range(n):
                if i + delta > n - 1:
                    break
                if not (i, i + delta) in distances:
                    min_distance = math.inf
                    for j in range(1, delta):
                        if (i, i + j) in distances and (i + j, i + delta) in distances:
                            min_distance = min(distances[(i, i + j)] + distances[(i + j, i + delta)], min_distance)
                    distances[(i, i + delta)] = min_distance
        return distances[(0, n - 1)]
    
    def jump2(self, nums):
        n = len(nums)
#        adj = [[] for _ in range(n)]
#        for i in range(n):
#            for j in range(nums[i]):
#                if i + j + 1 > n - 1:
#                    break
#                adj[i].append(i + j + 1)
        deque, distances, visited = collections.deque(), [math.inf] * n, set()
        deque.append(0)
        visited.add(0)
        distances[0] = 0
        while len(deque) > 0:
            cur_node = deque.popleft()
            if cur_node == n - 1:
                break
            for i in range(nums[cur_node]):
                node = cur_node + i + 1
                if node > n - 1:
                    break
                if not node in visited:
                    deque.append(node)
                    distances[node] = distances[cur_node] + 1
                    visited.add(node)
                    if node == n - 1:
                        break
        return distances[n - 1]
    
    def jump3(self, nums):
        n = len(nums)
        distances = [math.inf] * n
        distances[0] = 0
        min_step, max_step, flag = 1, min(nums[0], n - 1), 1
        while True:
            new_max = -1
            for i in range(min_step, max_step + 1):
                distances[i] = flag
                new_max = max(new_max, i + nums[i])
            flag += 1
            min_step = max_step + 1
            if min_step >= n:
                break
            if new_max <= max_step:
                break
            max_step = new_max
            if max_step >= n:
                max_step = n - 1
        return distances[n - 1]
            
            
                    
        