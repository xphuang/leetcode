# -*- coding: utf-8 -*-
"""
Created on Sat Sep 22 17:12:05 2018

Z字形变换
"""
class Solution:
    def convert(self, s, numRows):
        """
        :type s: str
        :type numRows: int
        :rtype: str
        """
        if numRows == 1:
            return s
        flag = 0 #衡量是向下走还是歇着方向(向上)走，0向下，1斜着（向上）
        strings = [[] for _ in range(numRows)]
        rownum = 0
        for c in s:
            if flag == 0:
                strings[rownum].append(c)
                rownum += 1
            else:
                strings[rownum].append(c)
                rownum -= 1
            if rownum == numRows:
                flag = 1
                rownum -= 2
            elif rownum == -1:
                flag = 0
                rownum = 1
        s_convert = ""
        for ss in strings:
            s_convert += ''.join(ss)
        return s_convert

