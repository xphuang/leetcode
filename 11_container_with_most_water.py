# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 16:11:50 2018

盛最多水的容器
"""

class Solution:
    
    def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        candidate_loc_left, candidate_loc_right = [], []
        initial_sum, n = -1, len(height)
        for i in range(n):
            if height[i] > initial_sum:
                candidate_loc_left.append(i)
                initial_sum = height[i]
        initial_sum = -1
        for i in range(n - 1, -1, -1):
            if height[i] > initial_sum:
                candidate_loc_right.append(i)
                initial_sum = height[i]
        max_area = 0
        for i in candidate_loc_left:
            for j in candidate_loc_right:
                max_area = max(abs(min(height[i], height[j]) * (i - j)), max_area)
        return max_area
    
    def maxArea2(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        new_height = [(height[_], _) for _ in range(len(height))]
        new_height.sort(key = lambda x: x[0])
        max_V = 0
        for i in range(len(new_height) - 1):
            max_V = max(max_V, max([abs(new_height[j][1] - new_height[i][1]) for j in range(i + 1, len(new_height))]) * new_height[i][0])
        return max_V
            

