# -*- coding: utf-8 -*-
"""
Created on Sat Oct  6 09:26:13 2018

字母异位词分组
"""

class Solution:
    def groupAnagrams(self, strs):
        """
        :type strs: List[str]
        :rtype: List[List[str]]
        """
        words_dict = {}
        for s in strs:
            s_sort = ''.join(sorted(s))
            if s_sort in words_dict:
                words_dict[s_sort].append(s)
            else:
                words_dict[s_sort] = [s]
        return list(words_dict.values())
    