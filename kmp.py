# -*- coding: utf-8 -*-
"""
Created on Tue Sep 25 14:01:03 2018

字符串KMP算法
"""


def getNext(pattern):
    n = len(pattern)
    nexts = [0] * n
    i, k, nexts[0] = 0, -1, -1
    while i < n - 1:
        if k == -1 or pattern[i] == pattern[k]:
            i, k = i + 1, k + 1
            if pattern[i] == pattern[k]:
                nexts[i] = nexts[k]
            else:
                nexts[i] = k
        else:
            k = nexts[k]
    return nexts
    
def kmp(string, pattern):
    nexts = getNext(pattern)
    i, j = 0, 0
    while i < len(string) and j < len(pattern):
        if len(string) - i < len(pattern) - j:
            return -1
        if j == -1 or string[i] == pattern[j]:
            i, j = i + 1, j + 1
        else:
            j = nexts[j]
        if j == len(pattern):
            return i - len(pattern)
    return -1
