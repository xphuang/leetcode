# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 11:06:00 2018

组合总和 II
"""

class Solution:
    
    def visit(self, cur_list, cur_sum, loc, candidates, target, result):
        if cur_sum == target:
            result.append(cur_list)
        else:
            for i in range(loc + 1, len(candidates)):
                if i > loc + 1 and candidates[i] == candidates[i - 1]:
                    continue
                if cur_sum + candidates[i] > target and candidates[i] > 0:
                    break
                self.visit(cur_list + [candidates[i]], cur_sum + candidates[i], \
                           i, candidates, target, result)
    
    def combinationSum2(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        candidates.sort()
        result = []
        for i in range(len(candidates)):
            if i > 0 and candidates[i] == candidates[i - 1]:
                continue
            if candidates[i] > target and candidates[i] > 0:
                break
            self.visit([candidates[i]], candidates[i], i, candidates, target, result)
        return result
