# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 16:55:19 2018

螺旋矩阵 II
"""

class Solution:
    def generateMatrix(self, n):
        """
        :type n: int
        :rtype: List[List[int]]
        """
        matrix = [[0] * n for _ in range(n)]
        row, col, rank, flag, num, cur = 0, -1, 0, 0, n * n, 0
        while cur < num:
            if flag == 0:
                col += 1
                if col == n - rank - 1:
                    flag = 1
            elif flag == 1:
                row += 1
                if row == n - rank - 1:
                    flag = 2
            elif flag == 2:
                col -= 1
                if col == rank:
                    flag = 3
            else:
                row -= 1
                if row == rank + 1:
                    flag = 0
                    rank += 1
            cur += 1
            matrix[row][col] = cur
        return matrix
