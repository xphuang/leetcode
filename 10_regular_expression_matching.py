# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 14:05:16 2018

正则表达式匹配
"""

class Solution:
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        s1 = "_" + s
        p1 = "_" + p
        matrix = [[0] * len(p1) for _ in range(len(s1))]
        for j in range(len(p1)):
            matrix[0][j] = 1
        for i in range(1, len(s1)):
            for j in range(1, len(p1)):
                if (p1[j] == '.' or p1[j] == s1[i]) and matrix[i - 1][j - 1] == 1:
                    matrix[i][j] = 1
                elif (p1[j] == '*' and p1[j - 1] == '.') or \
                (p1[j] == '*' and s1[i] == p1[j - 1] and (matrix[i - 1][j - 1] == 1 or matrix[i - 1][j] == 1)):
                    matrix[i][j] = 1
                elif p1[j] == s1[i] and p1[j - 1] == '*' and matrix[i - 1][j - 2] == 1:
                    matrix[i][j] = 1
        return matrix[-1][-1] == 1
        
        
