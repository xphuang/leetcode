# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 11:43:32 2018

通配符匹配
"""

class Solution:
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        if p == "*":
            return True
        s, p = '-' + s, '-' + p
        matrix = [[False] * len(p) for _ in range(len(s))]
        matrix[0][0] = True
        for i in range(1, len(s)):
            for j in range(1, len(p)):
                if p[j] == '*':
                    matrix[i][j] = matrix[i][j - 1] or matrix[i - 1][j - 1] or matrix[i - 1][j]
                else:
                    k = j - 1
                    while p[k] == '*':
                        k -= 1
                    matrix[i][j] = (matrix[i - 1][j - 1] and (p[j] == s[i] or p[j] == '?')) or \
                    (p[j - 1] == '*' and matrix[i - 1][k] and (p[j] == '?' or p[j] == s[i]))
        print(matrix)
        return matrix[-1][-1]
        
