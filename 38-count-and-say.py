# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 10:37:06 2018

报数
"""

class Solution:
    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """
        if n == 1:
            return '1'
        previous = '1'
        for i in range(1, n):
            new_previous, c, count = '', previous[0], 1
            for j in range(1, len(previous)):
                if previous[j] == c:
                    count += 1
                else:
                    new_previous += (str(count) + c)
                    count, c = 1, previous[j]
            new_previous += (str(count) + c)
            previous = new_previous
        return previous
