# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 22:34:38 2018

找第K小数字
"""

class Solution:
    def smallestDistancePair(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        def is_bigger_than(val, num_sorted, k):
            left, count = 0, 0
            for i, i_value in enumerate(num_sorted):
                while i_value - num_sorted[left] > val:
                    left += 1
                count += i - left
            return count >= k
        num_sorted = sorted(nums)
        left, right = 0, num_sorted[-1] - num_sorted[0] + 1
        while left < right:
            mid = int((left + right) / 2)
            if is_bigger_than(mid, num_sorted, k):
                right = mid
            else:
                left = mid + 1
        return left
        

