# -*- coding: utf-8 -*-
"""
Created on Sun Oct 14 09:36:41 2018

二进制求和
"""
class Solution:
    def addBinary(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        return bin(int(a, 2) + int(b, 2))[2:]
