# -*- coding: utf-8 -*-
"""
Created on Fri Oct  5 23:03:44 2018

全排列 II
"""

class Solution:
    def permuteUnique(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        result = []
        nums.sort()
        while True:
            flag, n = -1, len(nums)
            result.append(nums.copy())
            for i in range(n - 1):
                if nums[i] < nums[i + 1]:
                    flag = i
            if flag == -1:
                break
            else:
                min_more_index, min_more = flag + 1, nums[flag + 1]
                for i in range(flag + 2, n):
                    if nums[i] > nums[flag] and nums[i] <= min_more:
                        min_more_index, min_more = i, nums[i]
                    else:
                        break
                nums[flag], nums[min_more_index] = nums[min_more_index], nums[flag]
                a = nums[flag + 1:]
                nums[flag + 1:] = a[::-1]
        return result
        