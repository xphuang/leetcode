# -*- coding: utf-8 -*-
"""
Created on Tue Sep 25 20:47:28 2018

强连通图分支算法
"""

import collections

def tarjan_visit(start, adj, dfn, low, visited, visitedN, stack, branch_list):
    dfn[start] = len(visitedN)
    low[start] = len(visitedN)
    visitedN.append(start)
    visited[start] = True
    stack.append(start)
    for i in adj[start]:
        if dfn[i] == None:
            tarjan_visit(i, adj, dfn, low, visited, visitedN, stack, branch_list)
            low[start] = min(low[start], low[i])
        elif visited[i]:
            low[start] = min(low[start], low[i])
    if dfn[start] == low[start]:
        temp = []
        while len(stack) > 0:
            x = stack.pop()
            temp.append(x)
            visited[x] = False
            if x == start:
                break
        branch_list.append(temp)
    
    


def tarjan(adj):
    N = len(adj)
    dfn = [None] * N
    low = [None] * N
    visited = [False] * N #存储是否被访问过，以及被访问的序号
    visitedN = []
    stack = collections.deque()
    branch_list = []
    for i in range(N):
        if dfn[i] == None:
            tarjan_visit(i, adj, dfn, low, visited, visitedN, stack, branch_list)
    print(dfn)
    print(low)
    return branch_list
