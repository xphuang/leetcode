# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 19:53:06 2018

有效的括号字符串
"""

class Solution(object):
    def checkValidString(self, s):
        """
        :type s: str
        :rtype: bool
        """
        result = [0]
        result_set = set()
        result_set.add(0)
        for i, si in enumerate(s):
            if si == "(":
                for j in range(len(result)):
                    if result[j] < 0:
                        continue
                    result[j] += 1
                    result_set.remove(result[j] - 1)
                for j in range(len(result)):
                    result_set.add(result[j])
            elif si == ")":
                for j in range(len(result)):
                    if result[j] < 0:
                        continue
                    result[j] -= 1
                    result_set.remove(result[j] + 1)
                for j in range(len(result)):
                    result_set.add(result[j])
            else:
                for j in range(len(result)):
                    if result[j] < 0:
                        continue
                    if result[j] - 1 >= 0 and not result[j] - 1 in result_set:
                        result_set.add(result[j] - 1)
                        result.append(result[j] - 1)
                    if result[j] + 1 and not result[j] + 1 in result_set:
                        result_set.add(result[j] + 1)
                        result.append(result[j] + 1)
        if 0 in result_set:
            return True
        else:
            return False
        
    def checkValidString2(self, s):
        """
        :type s: str
        :rtype: bool
        """
        lower, upper = 0, 0
        for c in s:
            lower += 1 if c == '(' else -1
            upper -= 1 if c == ')' else -1
            if upper < 0:
                break
            lower = max(lower, 0)
        return lower == 0

