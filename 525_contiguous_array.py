# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 13:12:52 2018

连续数组
"""

class Solution(object):
    def findMaxLength2(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        n = len(nums)
        cum0 = [0] * (n + 1)
        cum1 = [0] * (n + 1)
        sum0, sum1 = 0, 0
        for i in range(n):
            if nums[i] == 0:
                sum0 += 1
            else:
                sum1 += 1
            cum0[i + 1] = sum0
            cum1[i + 1] = sum1
        max_len = 0
        for i in range(n):
            for j in range(i + 1, n):
                num0 = cum0[j + 1] - cum0[i]
                num1 = cum1[j + 1] - cum1[i]
                if num0 != num1:
                    continue
                max_len = max(max_len, num0)
        return 2 * max_len
    
    def findMaxLength(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        n = len(nums)
        cum_loc = {0 : -1}
        cur_sum = 0
        result = 0
        for i in range(n):
            cur_sum += 1 if nums[i] > 0 else -1
            if cur_sum in cum_loc:
                result = max(result, i - cum_loc[cur_sum])
            else:
                cum_loc[cur_sum] = i
        return result
        
