# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 17:10:23 2018

第k个排列
"""
import math

class Solution:
    def getPermutation(self, n, k):
        """
        :type n: int
        :type k: int
        :rtype: str
        """
        result, permutate_nums, flag = [], list(range(1, n + 1)), n
        while flag > 0:
            cur_all = math.factorial(flag - 1)
            remainder = k % cur_all
            quotient = k // cur_all
            if quotient == len(permutate_nums):
                result.extend(permutate_nums[::-1])
                break
            if remainder == 0:
                result.append(permutate_nums[quotient - 1])
                del permutate_nums[quotient - 1]
                result.extend(permutate_nums[::-1])
                break
            result.append(permutate_nums[quotient])
            del permutate_nums[quotient]
            flag -= 1
            k = remainder
        return ''.join([str(x) for x in result])
            